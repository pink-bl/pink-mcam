#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

# Prefix for all records
epicsEnvSet("PREFIX", "PINK:MCAMRX:")
epicsEnvSet("BL", "PINK")
epicsEnvSet("DEV", "MCAMRX")

# The port name for the detector
epicsEnvSet("PORT",   "NDSA")
# The queue size for all plugins
epicsEnvSet("QSIZE",  "5")
# The maximim image width; used to set the maximum size for this driver and for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",  "2592")
# The maximim image height; used to set the maximum size for this driver and for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",  "1944")
# The maximum number of time series points in the NDPluginStats plugin
epicsEnvSet("NCHANS", "2048")
# The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS", "5")
# The search path for database files
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db")
# The number of elements in the driver waveform record
epicsEnvSet("NELEMENTS", 5038848)
# The datatype of the waveform record
epicsEnvSet("FTVL", "UCHAR")
# The asyn interface waveform record
epicsEnvSet("TYPE", "Int8")

asynSetMinTimerPeriod(0.001)

# 2592*1944 = 5038848 * 4 = 40310784
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "41000000")

# Create an NDDriverStdArrays drivers
NDDriverStdArraysConfig("$(PORT)", $(QSIZE), 0, 0)

## Load record instances
dbLoadRecords("$(NDDRIVERSTDARRAYS)/db/NDDriverStdArrays.template","P=$(BL):,R=$(DEV):,PORT=$(PORT),ADDR=0,TIMEOUT=1,NELEMENTS=$(NELEMENTS),TYPE=$(TYPE),FTVL=$(FTVL)")

# Create a standard arrays plugin, set it to get data from the NDDriverStdArrays driver.
NDStdArraysConfigure("Image1", 3, 0, "$(PORT)", 0)
NDStdArraysConfigure("Image2", 3, 0, "$(PORT)", 0)
NDStdArraysConfigure("Image3", 3, 0, "$(PORT)", 0)
NDStdArraysConfigure("Image4", 3, 0, "$(PORT)", 0)

# This creates a waveform large enough for 1280x720x1 arrays.
# This waveform only allows transporting 8-bit images
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=USHORT,NELEMENTS=$(NELEMENTS)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image2:,PORT=Image2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=USHORT,NELEMENTS=$(NELEMENTS)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image3:,PORT=Image3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=USHORT,NELEMENTS=$(NELEMENTS)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image4:,PORT=Image4,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=USHORT,NELEMENTS=$(NELEMENTS)")

## Load record instances

cd "${TOP}/iocBoot/${IOC}"

## Load record instances
dbLoadRecords("${TOP}/iocBoot/${IOC}/mcamsup.db","BL=$(BL),DEV=$(DEV)")
dbLoadRecords("${TOP}/iocBoot/${IOC}/extra.db","BL=$(BL),DEV=$(DEV), BPM=sample")
dbLoadRecords("${TOP}/iocBoot/${IOC}/extra_linspace.db","BL=$(BL),DEV=$(DEV), BPM=sample")

# Load all other plugins using commonPlugins.cmd
< ${TOP}/iocBoot/${IOC}/commonPlugins.cmd

# autosave settings
set_requestfile_path("$(ADCORE)/ADApp/Db")
set_requestfile_path("${TOP}/iocBoot/${IOC}")
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

# autosave
create_monitor_set("auto_settings.req", 30, "P=$(PREFIX),BL=$(BL),DEV=$(DEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"

## set number of dimensions for the image
epicsThreadSleep(2.0)
dbpf("PINK:MCAMRX:NDimensions", "2")
